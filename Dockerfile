FROM registry.access.redhat.com/ubi8/ubi-minimal:8.7

ARG TARGETOS
ARG TARGETARCH

WORKDIR /root/
COPY ./bin/callback-${TARGETOS}-${TARGETARCH} ./callback

EXPOSE 80/tcp

ENTRYPOINT ["./callback"]
CMD ["start"]
