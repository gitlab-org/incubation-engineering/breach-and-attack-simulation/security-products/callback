package storage

type Storage interface {
	AllHits() []Hit
	Hits(key string) []Hit
	Track(key string) Hit
}
