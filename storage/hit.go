package storage

import (
	"strconv"
	"time"
)

type HitAttribute struct {
	Name  string
	Value string
}

type Hit struct {
	Key        string
	Attributes []HitAttribute
}

func NewTimestampAttribute() HitAttribute {
	return HitAttribute{
		Name:  "timestamp",
		Value: strconv.FormatInt(time.Now().UnixMilli(), 10),
	}
}
