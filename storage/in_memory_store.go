package storage

type InMemoryStore struct {
	store map[string][]Hit
}

func (s *InMemoryStore) AllHits() []Hit {
	result := []Hit{}
	for _, hits := range s.store {
		for _, hit := range hits {
			result = append(result, hit)
		}
	}

	return result
}

func (s *InMemoryStore) Hits(key string) []Hit {
	if _, ok := s.store[key]; !ok {
		s.store[key] = []Hit{}
	}

	return s.store[key]
}

func (s *InMemoryStore) Track(key string) Hit {
	attributes := []HitAttribute{
		NewTimestampAttribute(),
	}

	hit := Hit{
		Key:        key,
		Attributes: attributes,
	}

	s.store[key] = append(s.Hits(key), hit)

	return hit
}

func NewInMemoryStore() *InMemoryStore {
	return &InMemoryStore{
		store: map[string][]Hit{},
	}
}
