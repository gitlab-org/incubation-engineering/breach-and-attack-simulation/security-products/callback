package version

import (
	"runtime/debug"

	_ "embed"
)

func getBuildInfo(key string) string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == key {
				return setting.Value
			}
		}
	}

	return ""
}

//go:generate ../generate-git-tag.sh
//go:embed git-tag.txt
var BuildGitTag string

var BuildRevision = func() string {
	return getBuildInfo("vcs.revision")
}()

var BuildTime = func() string {
	return getBuildInfo("vcs.time")
}()
