package main

import (
	"gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/security-products/callback/cli"
)

func main() {
	cli.RunCLI()
}
