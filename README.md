# callback

Callback aims to create a simple docker build which accepts requests
from multiple protocols and records details about the client making
these requests.

This service is designed to be consumed by services or humans performing
Out-of-Band Application Security Testing (OAST).

:warning: The default configuration for callback will store request
statistics in-memory. This is not recommended for production use but
is the simplest way to run this application currently.

## Installation

Pull this docker image for this service from the GitLab Container Registry.

## Usage

Use `docker run` against this image to start the service listening for
on well-known ports for UDP, TCP, HTTP, and other traffic.

## Authors and acknowledgment

| Name        | GitLab |
|-------------|--------|
| Erran Carey | @erran |

## License

This project is licensed under the GitLab Enterprise Edition (EE) license (the “EE License”). See [LICENSE](./LICENSE).
