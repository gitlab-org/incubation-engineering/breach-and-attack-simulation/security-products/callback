package http

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/security-products/callback/storage"
)

type CallbackApp struct {
	Logger  *log.Logger
	Storage storage.Storage
}

type Error struct {
	Message string `json:"message"`
}

func (app *CallbackApp) HandleCallback(res http.ResponseWriter, req *http.Request) {
	correlationId := req.URL.Query().Get("correlation_id")
	app.Logger.Printf("HandleCallback - correlation_id: '%s'", correlationId)

	if len(correlationId) == 0 {
		WriteJsonResponse(res, http.StatusBadRequest, map[string]any{
			"errors": []Error{
				Error{Message: "correlation_id must be set"},
			},
		})
		return
	}

	hit := app.Storage.Track(correlationId)
	WriteJsonResponse(res, http.StatusCreated, map[string]any{
		"correlation_id": correlationId,
		"hit":            hit,
	})
}

func (app *CallbackApp) HandleStatisticsRequest(res http.ResponseWriter, req *http.Request) {
	correlationId := req.URL.Query().Get("correlation_id")
	app.Logger.Printf("HandleStatisticsRequest - correlation_id: '%s'", correlationId)

	var hits []storage.Hit
	if len(correlationId) == 0 {
		hits = app.Storage.AllHits()
	} else {
		hits = app.Storage.Hits(correlationId)
	}

	var status int
	if len(hits) == 0 {
		status = http.StatusNotFound
	} else {
		status = http.StatusOK
	}

	WriteJsonResponse(res, status, map[string]any{
		"hits": hits,
	})
}

func (app *CallbackApp) HandleStatus(res http.ResponseWriter, req *http.Request) {
	correlationId := req.URL.Query().Get("correlation_id")
	app.Logger.Printf("HandleStatus - correlation_id: '%s'", correlationId)

	WriteJsonResponse(res, http.StatusOK, map[string]any{
		"running": true,
	})
}

func NewServer(httpAddr string, logger *log.Logger) *http.Server {
	app := CallbackApp{
		Logger:  logger,
		Storage: storage.NewInMemoryStore(),
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/", app.HandleCallback)
	mux.HandleFunc("/-/requests", app.HandleStatisticsRequest)
	mux.HandleFunc("/-/status", app.HandleStatus)

	return &http.Server{
		Addr:         httpAddr,
		Handler:      mux,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
}

func WriteJsonResponse(res http.ResponseWriter, status int, v any) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(status)

	json.NewEncoder(res).Encode(v)
}
