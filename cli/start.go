package cli

import (
	"flag"
	"log"
	"os"

	"gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/security-products/callback/http"
)

type StartConfig struct {
	HTTPAddr *string
}

func RegisterStartSubcommand(cli *flag.FlagSet, args []string) StartConfig {
	start := flag.NewFlagSet("start", flag.ExitOnError)
	httpAddr := start.String("http-addr", ":80", "the 'host:port' to listen on")
	start.Parse(args)

	return StartConfig{
		HTTPAddr: httpAddr,
	}
}

func Start(startConfig StartConfig) {
	log := log.New(os.Stderr, "", log.LstdFlags|log.Lmicroseconds)
	app := http.NewServer(*startConfig.HTTPAddr, log)

	log.Printf("🚀 Listening on %+v\n", app.Addr)
	log.Fatal(app.ListenAndServe())
}
