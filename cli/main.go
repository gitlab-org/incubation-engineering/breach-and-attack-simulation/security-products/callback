package cli

import (
	"flag"
)

type Subcommand func()

func RegisterSubcommand(cli *flag.FlagSet, name string, cmd Subcommand) {
}

func RunCLI() {
	help := flag.Bool("help", false, "display usage")
	version := flag.Bool("version", false, "display version information")
	flag.Parse()

	args := flag.Args()
	var subcommand string
	if len(args) >= 1 {
		subcommand, args = args[0], args[1:]
	}

	if *help || subcommand == "help" {
		Help()
		return
	}

	if *version || subcommand == "version" {
		Version()
		return
	}

	switch subcommand {
	case "start":
		startFlags := RegisterStartSubcommand(flag.CommandLine, args)
		Start(startFlags)
		return
	default:
		Help()
		return
	}
}
