package cli

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/incubation-engineering/breach-and-attack-simulation/security-products/callback/version"

	_ "embed"
)

func Version() {
	details := strings.Join([]string{
		"revision " + version.BuildRevision,
		"time " + version.BuildTime,
	}, "; ")
	fmt.Printf("callback version %s (%s)", version.BuildGitTag, details)
}
